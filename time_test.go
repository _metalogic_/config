package config_test

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/_metalogic_/config"
)

var (
	lastFriday time.Time
	err        error
)

func TestGetDatetime(t *testing.T) {
	now := time.Now().UTC()
	ynow := now.Format("2006")
	// friday, err := conf.GetWeekday("Friday")
	if err != nil {
		t.Errorf("error: %s", err)
	}
	//mnow := now.Format("01")

	type expect struct {
		input  string
		want   string
		format string
	}

	tests := []expect{
		{input: "12", want: fmt.Sprintf("%s-12", ynow), format: "2006-01"},
		{input: "12-15", want: fmt.Sprintf("%s-12-15", ynow), format: "2006-01-02"},
		{input: "Fri", want: config.GetLast(time.Friday).Format("2006-01-02 15:04:05"), format: "2006-01-02 15:04:05"},
		{input: "Saturday", want: config.GetLast(time.Saturday).Format("2006-01-02 15:04:05"), format: "2006-01-02 15:04:05"},
	}

	for _, test := range tests {
		result, err := config.GetDatetime(test.input)
		if err != nil {
			t.Errorf("error: %s", err)
		}
		if result.Format(test.format) != test.want {
			t.Errorf("expected %s but got %s", test.want, result.Format(test.format))
		}
	}
}

func TestGetLast(t *testing.T) {
	now := time.Now()
	fmt.Printf("today is %s\n", now.Format("Monday"))
	for i := 0; i < 7; i++ {
		last := config.GetLast(time.Weekday(i))
		fmt.Printf("last %s is %s UTC\n", time.Weekday(i), last.Format("2006-01-02 15:04:05"))
	}
}

func TestGetWeekday(t *testing.T) {
	type expect struct {
		input string
		want  time.Weekday
	}

	tests := []expect{
		{input: "Sunday", want: time.Sunday},
		{input: "Sun", want: time.Sunday},
		{input: "monday", want: time.Monday},
		{input: "MoN", want: time.Monday},
		{input: "Tuesday", want: time.Tuesday},
		{input: "Tue", want: time.Tuesday},
		{input: "Wednesday", want: time.Wednesday},
		{input: "Wed", want: time.Wednesday},
		{input: "Thursday", want: time.Thursday},
		{input: "Thu", want: time.Thursday},
		{input: "Friday", want: time.Friday},
		{input: "Fri", want: time.Friday},
		{input: "Saturday", want: time.Saturday},
		{input: "Sat", want: time.Saturday},
	}

	for _, test := range tests {
		result, err := config.GetWeekday(test.input)
		if err != nil {
			t.Errorf("error: %s", err)
		}
		if result != test.want {
			t.Errorf("expected %s but got %s", test.want, result)
		}
	}
}
