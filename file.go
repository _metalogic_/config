package config

import (
	"fmt"
	"io/ioutil"
	"log/slog"
	"os"
	"path/filepath"
	"strings"
)

// FileFromSearchPath returns first valid file in searchpath
func FileFromSearchPath(searchpath string) (file *os.File, err error) {
	for _, c := range strings.Split(searchpath, ":") {
		var f string
		f, err = filepath.Abs(c)
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		file, err := os.Open(f) // For read access.
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		var stat os.FileInfo
		stat, err = file.Stat()
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		if stat.IsDir() {
			slog.Debug("found directory in file search path", "directory", c, "path", searchpath)
			continue
		}
		slog.Debug("found file in search path", "file", file.Name(), "path", searchpath)
		return file, nil
	}
	return file, fmt.Errorf("failed to get file from search path '%s'", searchpath)
}

// DirFromSearchPath returns the first valid directory in searchpath
func DirFromSearchPath(searchpath string) (dir *os.File, err error) {
	for _, c := range strings.Split(searchpath, ":") {
		var d string
		d, err = filepath.Abs(c)
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		dir, err = os.Open(d) // open directory for read access.
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		var stat os.FileInfo
		stat, err = dir.Stat()
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		if !stat.IsDir() {
			slog.Debug("found regular file in directory search path", "file", c, "path", searchpath)
			continue
		}
		slog.Debug("found directory in search path", "directory", dir.Name(), "path", searchpath)
		return dir, nil
	}
	return dir, fmt.Errorf("failed to get directory from search path '%s'", searchpath)
}

// LoadFromSearchPath returns the data read from the first valid file in searchpath
func LoadFromSearchPath(file, searchpath string) (data []byte, err error) {
	for _, c := range strings.Split(searchpath, ":") {
		var configFile string
		configFile, err = filepath.Abs(c + "/" + file)
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		data, err = ioutil.ReadFile(configFile)
		if err != nil {
			slog.Debug(err.Error())
			continue
		}
		slog.Debug("loaded data from file in search path", "file", file, "path", searchpath)
		return data, nil
	}
	return data, fmt.Errorf("failed to load data from file '%s' in search path '%s'", file, searchpath)
}
