package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log/slog"
	"os"

	"bitbucket.org/_metalogic_/config"
)

func main() {

	flag.Parse()

	logger := slog.New(slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{Level: slog.LevelDebug}))

	slog.SetDefault(logger)

	if len(flag.Args()) != 1 {
		logger.Error("usage: encrypt <secret>")
		os.Exit(1)
	}

	key := []byte(flag.Arg(0))
	if len(key) != 16 {
		slog.Error("encryption key must be 16 bytes")
		os.Exit(1)
	}

	slog.Debug("encrypting STDIN with secret key", "key", key)

	b, err := io.ReadAll(os.Stdin)
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}

	slog.Debug("read stdin", "input", string(b))

	encrypted, err := config.Encrypt(b, []byte(key))
	if err != nil {
		panic(fmt.Sprintf("unable to encrypt the data: %v", err))
	}

	writer := bufio.NewWriter(os.Stdout)
	count, err := writer.WriteString(encrypted)
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}
	if count != len(encrypted) {
		slog.Error("failed to write encrypted")
		os.Exit(1)
	}
	writer.Flush()
}
