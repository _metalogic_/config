package config

import (
	"crypto/rand"
	"fmt"
	"log/slog"
	"testing"
)

func TestEncryption(t *testing.T) {

	key, err := secret()
	if err != nil {
		panic(fmt.Sprintf("unable to create secret key: %v", err))
	}

	// encryption key must be 16 bytes
	key = []byte("0123456789ABCDEF")
	slog.Info("generated encryption key", "key", key)

	testCases := []struct {
		Message string
	}{
		{"hello"},
		{`Lorem Ipsum is simply dummy text of the printing and typesetting industry.
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
when an unknown printer took a galley of type and scrambled it to make a
type specimen book. It has survived not only five centuries, but also the
leap into electronic typesetting, remaining essentially unchanged. It was
popularised in the 1960s with the release of Letraset sheets containing
Lorem Ipsum passages, and more recently with desktop publishing software
like Aldus PageMaker including versions of Lorem Ipsum.`},
	}

	for _, tc := range testCases {
		t.Run(tc.Message, func(t *testing.T) {
			encrypted, err := Encrypt([]byte(tc.Message), key)
			if err != nil {
				t.Errorf("encrypt %s error: %s", tc.Message, err)
			}

			decrypted, err := Decrypt(encrypted, key)
			if err != nil {
				t.Errorf("decrypt %s error: %s", tc.Message, err)
			}
			if decrypted != tc.Message {
				t.Logf("%s != %s", tc.Message, decrypted)
			}
		})

	}

}

func secret() ([]byte, error) {
	key := make([]byte, 16)

	if _, err := rand.Read(key); err != nil {
		return nil, err
	}

	return key, nil
}
