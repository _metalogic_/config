package config

import (
	"log/slog"
	"os"
)

func init() {
	logger := slog.New(slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{Level: slog.LevelDebug}))
        slog.SetDefault(logger)
}

func SetLogger(logger *slog.Logger) {
	slog.SetDefault(logger)
}
